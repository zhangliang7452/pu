package com.xml.guard.tasks

import com.xml.guard.entensions.GuardExtension
import com.xml.guard.utils.allDependencyAndroidProjects
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import javax.inject.Inject

open class AllTask @Inject constructor(
    private val guardExtension: GuardExtension
) : DefaultTask() {

    init {
        group = "guard"
    }

    @TaskAction
    fun execute() {
        val rename = project.tasks.getByName("rename") as RenameTask
        val packageChange = project.tasks.getByName("packageChange") as PackageChangeTask
        val xmlClassGuard = project.tasks.getByName("xmlClassGuard") as XmlClassGuardTask
        val findAllDirToGuard = project.tasks.getByName("findAllDirToGuard") as FindAllDirToGuardTask
//      rename.mustRunAfter(packageChange)
//      packageChange.mustRunAfter(findAllDirToGuard)
//      findAllDirToGuard.mustRunAfter(xmlClassGuard)
        rename.execute()

        println("rename 2 completed")
        packageChange.execute()

        println("packageChange 2 completed")
        findAllDirToGuard.execute()

        println("findAllDirToGuard 2 completed")
        xmlClassGuard.isRef = true
        xmlClassGuard.execute()
        println("xmlClassGuard 2 completed")
    }
}