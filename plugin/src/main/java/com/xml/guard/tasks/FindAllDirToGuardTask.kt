package com.xml.guard.tasks

import com.android.build.gradle.BaseExtension
import com.xml.guard.entensions.GuardExtension
import com.xml.guard.model.MappingParser
import com.xml.guard.utils.allDependencyAndroidProjects
import com.xml.guard.utils.findPackage
import com.xml.guard.utils.getDirPath
import com.xml.guard.utils.javaDirs
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.File
import javax.inject.Inject


//查找全部可混淆根目录并生成对应混淆目录
open class FindAllDirToGuardTask @Inject constructor(
    private val guardExtension: GuardExtension
) : DefaultTask() {

    private val mappingFile = guardExtension.mappingFile ?: project.file("xml-class-mapping.txt")
    private val mapping = MappingParser.parse(mappingFile)

    init {
        group = "guard"
    }


    @TaskAction
    fun execute() {
        println("zhangliang  -----start------------FindAllDirToGuardTask---------")
        mapping.dirMapping.clear();
        mapping.classMapping.clear();
        val androidProjects = allDependencyAndroidProjects()
        androidProjects.forEach {
            it.javaDirs().forEach { a ->
                val mdir = a.absolutePath + File.separator;
                val walk: FileTreeWalk = a.walk()
                walk.forEach { b ->
                    if (b.isFile) {
                        val ldir = b.parent;
                        val aa = ldir.replace(mdir, "").replace(File.separator, ".")
                        val fi = guardExtension.filterateDir
                        if (!mapping.dirMapping.containsKey(aa) && !fi.contains(aa)) {
                            println("zhangliang  -----add------------$aa")
                            mapping.obfuscatePackage(aa)
                        }
                    }
                }
            }

        }
        mapping.writeMappingToFile(mappingFile)
    }
}