package com.xml.guard.tasks

import com.android.build.gradle.BaseExtension
import com.xml.guard.entensions.GuardExtension
import com.xml.guard.model.MappingParser
import com.xml.guard.utils.allDependencyAndroidProjects
import com.xml.guard.utils.findPackage
import com.xml.guard.utils.getDirPath
import com.xml.guard.utils.javaDirs
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.File
import javax.inject.Inject


//查修改指定的文件字段
open class RenameTask @Inject constructor(
    private val guardExtension: GuardExtension
) : DefaultTask() {

    init {
        group = "guard"
    }

    @TaskAction
    fun execute() {
        val pakge = guardExtension.renamePackage ?: return
        val projectDir = project.rootDir.absolutePath+File.separator
        val fi = guardExtension.rename
        for (fiit in fi) {
            println("zhangliang  -----Rename---------$fiit")
            val newFile = File(projectDir + fiit)
            if (!newFile.exists()) {
                continue
            }
            var replaceText = newFile.readText()
            replaceText = replaceText.replace("public_name_", pakge)
            newFile.writeText(replaceText)
            if (newFile.absolutePath.contains("public_name_")) {
                val namefile = File(newFile.absolutePath.replace("public_name_", pakge))
                newFile.renameTo(namefile)
            }
        }

    }
}